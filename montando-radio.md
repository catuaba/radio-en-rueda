# Criando uma radio ambulante
  Como ando andando por aí, decidi levar um pouco de história junto, aí em vez de contar as histórias das pessoas com as minhas palavras, escolhi que elas mesmo devem contar, ou seja, de certa maneira levarei as vozes das pessoas comigo, pra onde? Ainda não sei...
  A ideia é que cada vez que estiver parada em algum lugar, ligarei a rádio e esta estará disponível através de uma rede wifi aberta. Assim que uma pessoa se conectar nessa rede, automaticamente abrirá uma pagina web (portal captivo) no dispositivo da pessoa, com o player da radio.

## Equipamentos
  RadioEnRueda: Raspberry Pi Model B
  Cajita: roteador TP-link MR3020v1
  Gravador: Celular Android Alcatel

## Configurando a RadioEnRueda
  Instalado projeto [DietPi](dietpi.com) na Rasp. Esse projeto é um sistema operacional mais leve para a Rasp, baseado na Raspian (Debian), que inclui um gerenciamento de softwares criado pela galera do projeto com uma curadoria de softwares para a DietPi
  Para criar a radio, escolhi o projeto [IceCast](https://icecast.org) como serviço de streaming.
  Defini o hostname como 'radioenrueda'.
  Configurei o mountpoint radio-en-rueda.ogg como padrão, onde cada transmissão encerrada será salva em um diretório chamado historias. Quando não a rádio não estiver ao vivo, transmitirá as suas histórias (fallback).
  Configurei um mountpoint chamado historias.ogg que está configurado como oculto (hidden) para não ser listado no diretório de mountpoint da rádio, caso alguém acesse a página do IceCast através da rede.
  Para transmitir as historias como fallback, estou usando o software [Ezstream](https://icecast.org/ezstream/), configurado para fazer stream de uma playlist, apontando para o mountpoint historias.ogg.
  Configurei o crontab para criar a playlist e executar ezstream cada vez que ligar a Radio.
//TODO - links para os arquivos de configuracao


## Configurando o roteador (Cajita)
  Baixei a imagem OpenWrt versão 17.01.7 que era a última suportada para a Cajita.
  Como portal captivo, escolhi o pacote NoDogSplash por ser o mais leve.
  Depois de instalado o NoDogSplash, editei sua página splash.html para incluir o player da radio.
  Como esse será um roteador "offline", sem acesso à internet, configurei um endereço específico para a máscara de DNS, para o serviço de DHCP:
    uci add_list dhcp.@dnsmasq[0].address = '/#/192.168.1.1'
    uci commit dhcp
    service dnsmasq restart
  E ativei a rede wifi, com o nome "Escuche-ME"

## Celular
  Instalado aplicativo [CoolMic](https://coolmic.net/) para gravar e transmitir as histórias para a servidora da rádio. Configurei para fazer a transmissão ao mountpoint radio-en-rueda.ogg, conectado na servidora radioenrueda:8000

## Teste no meu computador
### Browser
    Quando se conecta a rede Escoche-ME, abre o portal captivo, exibe o player, toca assim que pressionado o play e para de tocar quando acaba cada "história", ou seja, não segue tocando o stream, precisa recarregar a pagina

### Celluloid
    Ouvir a rádio em um cliente funciona melhor, enquanto existe a transmissão ao vivo, se escuta bem e quando para a transmissão ao vivo, automaticamente o stream muda para as histórias salvas.

## Teste no celular do Santi
  Abre o portal captivo automaticamente quando ele conecta na rede Escuche-ME, exibe o player e quando pressionado play, o stream executa normalmente sem necessidade de recarregar a página e quando a rádio muda de offline para online, tudo funciona como deveria funcionar.

## Teste Familia Ferreira
  Em uma saída com a familia Ferreira, fizemos uma gravacão dentro do carro. Radio, Cajita e celular ligados e conectados na rede da Cajita.
  Quando chegamos ao parque, liguei os equipamentos e pedi para as pessoas conectarem à rede Escuche-ME.
  Tive que reiniciar os equipamentos 4 ou 5 vezes até conseguirem conectar à rede. As vezes a rede não entregava IP, as vezes a Radio não carregava. Por esse problema de vai mas não vai, precisei pedir para as pessoas apagarem (esquecer) a rede wifi nos seus celulares.
  Quando tudo funcionou (testei no meu celular primeiro), pedi que conectassem à rede, o portal captivo abriu em todos os dispositivos automaticamente, mas nos iPhones não exibia o player da rádio, ou seja, a tag video não era renderizada.

## Avaliação
  Como a imagem OpenWrt veio com portal LUCI e vários outros pacotes, no final ocupou 86% da memoria da Cajita, não persistindo mais edicões como mudança de SSID ou permitindo edicão extra na página splash.html. Nessa página tem um botão de "autenticação" que eu não preciso mais, como não pude removê-lo, era exibido e as pessoas clicavam aí.
  A rádio não foi exibida nos celulares iPhone, preciso de uma forma de testar isso, talvez configurando o navegador para se comportar como iPhone.
  Tenho um powerbank que serviu para alimentar todos os equipamentos durante o teste, falta testar quanto tempo dura fazendo transmissão e gravando histórias.



## Configurando a Cajita - v2
  Como a ultima configuracao ocupava muito espaco, estou compilando um firmware para a Cajita sem a interface LUCI, aproveitando para adicionar por padrao o pacote NoDogSplash e pacotes para o uso do modem 3G

Compilando firmware
  Download source code OpenWrt v17.01.7 (https://github.com/openwrt/openwrt/releases/tag/v17.01.7)
  Compilando...
    ./scripts/feed update -a
    ./scripts/feed install -a
    make defconfig
    make menuconfig
      Selecionado
        Target ar71xx
        Profile TLMR3020
        Pacotes:
          block-mount
          kmod-fs-ext4
          kmod-lib-crc32c
          kmod-usb-net-cdc-ether
          kmod-usb-ohci
          kmod-usb-storage
          kmod-usb-uhci
          kmod-usb2
          nodogsplash
          usb-modeswitch

  Cajita no modo online
    uci delete dhcp.@dnsmasq[0].address
